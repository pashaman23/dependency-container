<?php

require __DIR__ . '/vendor/autoload.php';

//Using Container
$container = new Container\Container();

$container->bind('test', function () {
    return new stdClass();
});

$container->bind('db', function () {
    return new \PDO('mysql:localhost;dbname=db', 'username', 'password');
});

print_r($container->get('test'));