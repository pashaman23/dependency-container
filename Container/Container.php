<?php

namespace Container;

class Container
{
    private $services = [];
    private $cache = [];

    public function bind($serviceName, $value)
    {
        $isServiceExistsInCache = $this->isServiceExistsInCache($serviceName);
        if ($isServiceExistsInCache) {
            unset($this->cache[$serviceName]);
        }

        $this->services[$serviceName] = $value;
    }

    public function get($serviceName)
    {
        $isServiceExistsInCache = $this->isServiceExistsInCache($serviceName);

        if ($isServiceExistsInCache) {
            return $this->cache[$serviceName];
        }

        $isServiceExists = $this->isServiceExists($serviceName);

        if ($isServiceExists === false) {
            throw new \Exception("The service: $serviceName does not exists.");
        }

        $service = $this->services[$serviceName];

        if ($service instanceof \Closure) {
            $this->cache[$serviceName] = $service();
        } else {
            $this->cache[$serviceName] = $service;
        }

        return $this->cache[$serviceName];
    }

    protected function isServiceExists($serviceName)
    {
        return array_key_exists($serviceName, $this->services);

    }

    protected function isServiceExistsInCache($serviceName)
    {
        return array_key_exists($serviceName, $this->cache);
    }
}
